const fs = require('fs')
const deleteImage = (path, image) => {
    let full_path = "public/uploads/"+path
    try{
        if(image){
            if(fs.existsSync(full_path+'/'+image)){
                let result = fs.rmSync(full_path+'/'+image)
                return result
            }else{
                return null;
            }
        }
    } catch(err){
        throw {status : 400, msg : err }
    }
}

module.exports = {
    deleteImage
}