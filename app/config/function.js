const generateRandomString  = (len = 100) => {
    let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    let strlen = chars.length
    let random = ''

    for(let i = 0; i < len; i++){
        let random_num = parseInt((Math.random()) * strlen)
        random += chars[random_num];
    }
    return random
}

module.exports = {
    generateRandomString
}