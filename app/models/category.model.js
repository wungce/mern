const mongoose = require('mongoose')
const {StatusSchema} = require('./common/status.schema')

const CategorySchema = new mongoose.Schema({
    title : {
        type : String,
        required : true,
        unique : true,
    },
    slug : {
        type : String,
        required : true,
        unique : true
    },
    child_of : {
        type : mongoose.Types.ObjectId,
        ref : "Category",
        default : null
    },
    image : {
        type : String,
    },
    brands : [{
        type : mongoose.Types.ObjectId,
        ref : "Brand"
    }],
    status : StatusSchema,
    show_in_home : {
        type : Boolean,
        default : false
    }
},{
    autoIndex : true,
    autoCreate : true,
    timestamps : true
})
const CategoryModel = mongoose.model("Category", CategorySchema)
module.exports = CategoryModel