const CategoryService = require('../services/category.service')
const {deleteImage} = require('../helpers/function')
const slugify = require('slugify')
class CategoryController {
    constructor(){
        this.category_srv = new CategoryService()
    }
    addCategory = async (req, res, next) => {
        try{
            let data = req.body
            if(req.file) {
                data.image = req.file.filename
            }
            this.category_srv.validationCategory(data)

            data.slug  = slugify(data.title, {replacement : "-", lower : true})
            if(!data.child_of){
                data.child_of = null
            }
          let ack = await  this.category_srv.createCategory(data)
            res.json({
                msg : "Category created successfully.",
                status : true,
                result : data
            })

        } catch(err){
            next(err)
        }
    }
    categoryList = async (req, res, next) => {
       try{
        let allCategoryList = await this.category_srv.categoryList()
        res.json({
                    result:allCategoryList,
                    msg : "All categories are fetched.",
                    status: true
                  })
       } catch(err){
        next(err)
       }
    }
    getById = async (req, res, next) => {
        try{
            let categorys = await this.category_srv.getCategoryById(req.params.id)
            res.json({
                        result:categorys,
                        msg : "Categorys fetched successfully.",
                        status: true
                      })
           } catch(err){
            next(err)
           }
    }
    updateCategory = async (req, res, next) => {
        try{
            let data = req.body
            // console.log(req.file)
            if(req.file) {
                data.image = req.file.filename
            }
            this.category_srv. validationCategory(data)
            if(!data.child_of){
                data.child_of = null
            }   
           this.category_srv.updateCategoryById(data, req.params.id)
            res.json({
                result : data,
                status : true,
                msg : "Category update successfully."
            })

        } catch(err){
            next(err)
        }
    }
    deleteById = async (req, res, next) => {
        try{
            let ack = await this.category_srv.deleteCategoryById(req.params.id)
            if(ack){
                deleteImage('category', ack.image)
            }
            res.json({
                result : ack,
                status : true,
                msg : "Category deleted successfully."
            })
        } catch(err){
            next(err)
        }
    }
}

module.exports = CategoryController
