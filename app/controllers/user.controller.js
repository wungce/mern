const db = require('../services/mongodb.service')
const AuthService = require('../services/auth.service')
const mongodb = require('mongodb')
class UserController {
    constructor(){
        this.auth_srv = new AuthService()
    }
    userList = async(req, res, next) => {
        try{
            let selected_db = await db()
            if(selected_db){
                let users = await selected_db.collection('users').find().toArray()
                res.json({
                    result : users,
                    status : true,
                    msg : "Data fetch."
                })
            }else{
                throw "Error while db connection."
            }

        } catch(err){
            console.log(err)
            next({
                status : 500,
                msg : err
            })

        }
    }
    updateUser = async(req, res, next) => {
        try{
            let data = req.body
            if(req.file){
                data.image = req.file.filename
            }
            let validate = this.auth_srv.registerValidation(data, true)
            if(validate){
                next({
                    msg : validate,
                    status : 400 
                })
            }else{
                let selected_db = await db()
                if(selected_db){
                    // selected_db.collection('users').updateOne({
                    //     _id: new mongodb.ObjectId(req.params.id)
                    // }, {
                    //     $set : data
                    // })
                    // .then((respo) => {
                    //     res.json({
                    //         result: data,
                    //         status : true,
                    //         msg : "User profile updated."
                    //     })
                    // })
                    // .catch((err) => {
                    //     next({
                    //         status :400,
                    //         msg : err
                    //     })
                    // })
                    try{
                         selected_db.collection('users').updateOne({
                                 _id: new mongodb.ObjectId(req.params.id)
                            }, {
                             $set : data
                            })
                            res.json({
                                     result: data,
                                     status : true,
                                     msg : "User profile updated."
                                 })

                    } catch(err){
                        next({
                            status :400,
                            msg : err
                        })
                    }
                }else{
                    throw "Error establish connection."
                }
            }
        } catch(err){
            console.log(err)
            next({
                status : 500,
                msg : err
            })
        }
    }
    userDelete = async(req, res, next) => {
        try{
            let selected_db = await db()
            if(selected_db){
                let users = await selected_db.collection('users').deleteOne({
                    _id: new mongodb.ObjectId(req.params.id)
                })
                res.json({
                    result : null,
                    status : true,
                    msg : "Delete user."
                })
            }else{
                throw "Error while db connection."
            }

        } catch(err){
            next({
                msg : err,
                status : 500
            })
        }
    }
    getDetail = async(req, res, next) => {
        try{
            let user = await this.auth_srv.getUserById(req.params.id) 
            if(user){
                res.json({
                    result : user,
                    msg : "user Fetched.",
                    status : true
                })
            }else{
                next({
                    status : 404,
                    msg : "User does not exists."
                })
            }
        } catch(err){
            next(err)
        }
    }
}

module.exports = UserController