const UserModel = require('../models/user.model')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const CONFIG = require('../config/config')

class AuthService {
    loginValidation = (data) => {
        let msg = null
        if(!data.email && !data.password){
            msg = "Credentials are required."
        }else{
            msg = null
        }
        return msg
    }
    registerValidation = (data, isUpdate = false) => {
        console.log(isUpdate)
        let msg = {};
        if(!data.name){
            msg['name'] = "Name is required."
        }
        if(!data.email){
            msg['email'] = "Email is required."
        }
        if(!isUpdate){
            console.log("Test here")
            if(!data.password){
                msg['password'] = "Password is required."
            }
        }
        if(!data.role){
            msg['role'] = "Role is required."
        }else{
            if(data.role !== 'admin' && data.role !== 'seller' && data.role !== 'Customer'){
                msg['role'] = "Admin, seller or customer default set."
            }
        }
        if(Object.keys(msg).length > 0){
            throw msg
        }else{
            return null
        } 
    }
    registerUser = (data) => {
        let user = new UserModel(data)
        return user.save()
    }
    login = async(data) => {
        try{
            let user = await UserModel.findOne({
                email : data.email,
            })
            if(user){
                if(bcrypt.compareSync(data.password, user.password)){
                    return user
                }else{
                    throw {status : 400, msg : "Credential does not match."}
                }
            }else{
                throw{status: 400, msg :'user dose not exists.'}
            }
        } catch(err){
            throw err
        }
    }
    
    getToken = (data) => {   // one parameter is payload
        let token = jwt.sign(data, CONFIG.JWT_SECRET)
        return token 
    }
    getUserById = async (id)  => {
        try{
            let user = await UserModel.findById(id)
            return user
        } catch(err){
            throw err
        }
    }
}

module.exports = AuthService