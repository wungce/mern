const ProductModel = require('../models/product.model')

class ProductService {
    validationProduct = (data) => {
        let err_msg = {}
        if(!data.title){
            err_msg.name = "Name is required."
        }
        if(!data.price){
            err_msg.price = "Price is required."
        }

        let status = ['active', 'inactive', 'out-of-stock']

        if(!data.status){
            err_msg.status = "Status is required."
        }else if(!status.includes(data.status)){
            err_msg.status = "Status can be either active, inactive or out-of-stock."
        }

        if(Object.keys(err_msg).length > 0){
            throw {status : 400, msg : err_msg}
        }else{
            return null
        }
    }
    createProduct = async(data) => {
        try{
            let product = new ProductModel(data)
            let ack = await product.save()
            if(ack){
                return product
            }else{
                throw {status : 400, msg :"Error creating product."}
            }
        } catch(err){
            throw err
        }
    }
    productList = async() => {
            return await ProductModel.find()
            .populate("seller")
            .populate("category")
            .populate("brand")
    }
    getProductById = async (id) => {
        return await ProductModel.findById(id);
    }
    updateProductById = async (data, id) => {
        try{
            return await ProductModel.findByIdAndUpdate(id, {
                $set : data
            })
        } catch(err){
            throw {status : 400, msg : err}

        }
    }
    deleteProductById = async (id) => {
        try{
            let ack =  await ProductModel.findByIdAndRemove(id)
            if(ack){
                return ack
            }else{
                throw "Product already delete."
            }
        } catch(err){
            throw {status : 500, msg : err}
        }
    }
    generateUniqueSlug = async(slug) => {
        let exists = await ProductModel.findOne({
            slug : slug
        })
        if(exists){
            slug = slug+Date.now()
            await this.generateUniqueSlug(slug)
        }else {
            return slug
        }
    }
}

module.exports = ProductService