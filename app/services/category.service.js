const CategoryModel = require('../models/category.model')

class CategoryService {
    validationCategory = (data) => {
        let err_msg = {}
        if(!data.title){
            err_msg.name = "Name is required."
        }
        if(!data.status){
            err_msg.status = "Status is required."
        }else if(data.status !== 'active' && data.status !== 'inactive'){
            err_msg.status = "Status can be either active or inactive."
        }
        if(Object.keys(err_msg).length > 0){
            throw {status : 400, msg : err_msg}
        }else{
            return null
        }
    }
    createCategory = async(data) => {
        try{
            let category = new CategoryModel(data)
            let ack = await category.save()
            if(ack){
                return category
            }else{
                throw {status : 400, msg :"Error creating category."}
            }
        } catch(err){
            throw err
        }
    }
    categoryList = async() => {
        try{
            return await CategoryModel.find().populate("brands")
            .populate("child_of")
        } catch(err){
            throw {msg : "Category does not store.", status : 422}
        }
    }
    getCategoryById = async (id) => {
        return await CategoryModel.findById(id);
    }
    updateCategoryById = async (data, id) => {
        try{
            return await CategoryModel.findByIdAndUpdate(id, {
                $set : data
            })
        } catch(err){
            throw {status : 400, msg : err}

        }
    }
    deleteCategoryById = async (id) => {
        try{
            let ack =  await CategoryModel.findByIdAndRemove(id)
            if(ack){
                return ack
            }else{
                throw "Category already delete."
            }
        } catch(err){
            throw {status : 500, msg : err}
        }
    }
}

module.exports = CategoryService