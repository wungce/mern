const BrandModel = require('../models/brand.model')

class BrandService {
    validationBrand = (data) => {
        let err_msg = {}
        if(!data.name){
            err_msg.name = "Name is required."
        }
        if(!data.status){
            err_msg.status = "Status is required."
        }else if(data.status !== 'active' && data.status !== 'inactive'){
            err_msg.status = "Status can be either active or inactive."
        }
        if(Object.keys(err_msg).length > 0){
            throw {status : 400, msg : err_msg}
        }else{
            return null
        }
    }
    createBrand = async(data) => {
        try{
            let brand = new BrandModel(data)
            let ack = await brand.save()
            if(ack){
                return brand
            }else{
                throw {status : 400, msg :"Error creating brand."}
            }
        } catch(err){
            throw err
        }
    }
    brandList = async() => {
        try{
            return await BrandModel.find()
        } catch(err){
            throw {msg : "Brand does not store.", status : 422}
        }
    }
    getBrandById = async (id) => {
        return await BrandModel.findById(id);
    }
    updateBrandById = async (data, id) => {
        try{
            return await BrandModel.findByIdAndUpdate(id, {
                $set : data
            })
        } catch(err){
            throw {status : 400, msg : err}

        }
    }
    deleteBrandById = async (id) => {
        try{
            let ack =  await BrandModel.findByIdAndRemove(id)
            if(ack){
                return ack
            }else{
                throw "Brand already delete."
            }
        } catch(err){
            throw {status : 500, msg : err}
        }
    }
}

module.exports = BrandService