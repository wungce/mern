const express = require('express')
const http = require('http')
const app = express()
const socketServer = http.createServer(app)
const routes = require('./routes/routes')
// require('./app/service/mongoose.service')
// require('./app/event/myevent.event')
require('./app/services/mongoose.service')

const cors = require('cors')
app.use(cors())

const myEvent = require('./app/event/myevent')
const { Server } = require("socket.io");
const io = new Server(socketServer);

io.on('connection', (socket) => {
    console.log('a user connected');
  });

// event
app.use((req, res, next) => {
    req.myEvent = myEvent
    next()
})

app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use("/images", express.static(process.cwd()+"/public/uploads"))

app.use('/api/v1', routes)



// error handle middleware
app.use((req,res,next) => {
    next({
        msg : "page not found",
        status : 404
    })
})
app.use((error, req, res, next) =>{
    let  status_code = error.status || 500
    let  msg = error.msg || error
        res.status(status_code).json(
        {
            msg : msg,
            result : null,  
            status : false,

        }
    )
})
app.listen(3005, 'localhost',(err) => {
    if(err){
        console.log("Error does not listening")
    }else{
        console.log("This port is listening here.")
    }
})
