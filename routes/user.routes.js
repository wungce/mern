const router = require('express').Router()
const UserController = require('../app/controllers/user.controller')
const user_ctrl = new UserController()
const uploader = require('../app/middleware/uploader.middleware')
const loginCheck = require('../app/middleware/auth.middleware')

const isAdmin = (req, res, next) => {
    next()
}
router.route('/')
.get(loginCheck, isAdmin, user_ctrl.userList)

router.route('/:id')
.get(loginCheck, user_ctrl.getDetail)
.put(uploader.single('image'),user_ctrl.updateUser)
.delete(loginCheck, isAdmin, user_ctrl.userDelete)

module.exports = router